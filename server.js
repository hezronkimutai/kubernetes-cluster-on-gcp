var express = require('express');

const os = require('os');
var app = express();
app.get('/', function (req, res) {
  res.send("You're on, " + os.hostname() + "\n"); 
});
app.listen(3000, function () {
	console.log('Listening on port 3000!'); 
	console.log(' http://localhost:3000');
});
